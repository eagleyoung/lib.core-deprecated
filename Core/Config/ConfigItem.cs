﻿using System.Collections.Generic;

namespace Lib.Core
{
    /// <summary>
    /// Данные конфига
    /// </summary>
    public class ConfigItem
    {
        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; }

        /// <summary>
        /// Целочисленное значение
        /// </summary>
        public int Int
        {
            get
            {
                int value = 0;
                if(!int.TryParse(Value, out value))
                {
                    OutTrace.Log($"Ошибка при преобразовании значения в int ({Value})");
                }
                return value;
            }
        }

        /// <summary>
        /// Булевое значение
        /// </summary>
        public bool Bool
        {
            get
            {
                bool value = false;
                if(!bool.TryParse(Value, out value))
                {
                    OutTrace.Log($"Ошибка при преобразовании значения в bool ({Value})");
                }
                return value;
            }
        }

        /// <summary>
        /// Список строк при разделении через точку с запятой (;)
        /// </summary>
        public List<string> Strings
        {
            get
            {
                var output = new List<string>();
                foreach (var item in Value.Split(new char[] { ';' }))
                {
                    var trim = item.Trim();
                    if (!string.IsNullOrEmpty(trim))
                    {
                        output.Add(trim);
                    }
                }
                return output;
            }
        }

        public ConfigItem(string _value)
        {
            Value = _value;
        }
    }
}
