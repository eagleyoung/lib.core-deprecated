﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Lib.Core
{
    /// <summary>
    /// Класс для работы с конфигурационным файлом config.ini
    /// </summary>
    public static class Config
    {
        static Dictionary<string, ConfigItem> data = new Dictionary<string, ConfigItem>();
        /// <summary>
        /// Получить параметр по его ключу. 
        /// При отсутствии параметра возвращает null.
        /// </summary>
        public static ConfigItem ByKey(string key)
        {
            if (!data.TryGetValue(key, out ConfigItem value))
            {
                Output.Warning($"Отсутствует параметр {key} в конфигурационных файлах");
            }
            return value;
        }

        static Config()
        {
            var assembly = Assembly.GetEntryAssembly();
            if(assembly != null)
            {
                var cfgFolderPath = $"{Path.GetDirectoryName(assembly.Location)}\\settings\\cfg";
                var cfgFolder = new DirectoryInfo(cfgFolderPath);
                if (!cfgFolder.Exists)
                {
                    try
                    {
                        cfgFolder.Create();
                    }catch(Exception ex)
                    {
                        OutTrace.Log("Ошибка при создании директории для конфигурационных файлов", ex);
                        return;
                    }
                }

                var cfgFiles = cfgFolder.GetFiles("*.ini", SearchOption.TopDirectoryOnly);
                foreach(var file in cfgFiles)
                {
                    var name = file.Name.Substring(0, file.Name.Length - file.Extension.Length);
                    foreach (var line in File.ReadAllLines(file.FullName))
                    {
                        var trim = line.Trim();
                        if (trim.StartsWith("//") || trim.Length == 0) continue;

                        var equalIndex = trim.IndexOf('=');
                        if (equalIndex >= 0)
                        {
                            var dataKey = trim.Substring(0, equalIndex).Trim();
                            var key = $"{name}.{dataKey}";
                            var value = trim.Substring(equalIndex + 1).Trim();
                            if (!data.ContainsKey(key))
                            {
                                data.Add(key, new ConfigItem(value));
                            }
                            else
                            {
                                data[key] = new ConfigItem(value);
                                OutTrace.Log($"Дублирование ключа {key} в конфигурационном файле {file.Name}");
                            }
                        }
                    }
                }
            }            
        }
    }
}
