﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Lib.Core
{
    /// <summary>
    /// Стандартный вывод данных
    /// </summary>
    public static class Output
    {
        static Lazy<TextWriter> writer = new Lazy<TextWriter>(() => {
            var assembly = Assembly.GetEntryAssembly();
            if (assembly != null)
            {
                var fileOutput = $"{Path.GetDirectoryName(assembly.Location)}\\log\\{Environment.UserName}_{DateTime.Now.Writable()}.txt";
                var info = new FileInfo(fileOutput);
                if (!info.Directory.Exists)
                {
                    try
                    {
                        info.Directory.Create();
                    }
                    catch (Exception ex)
                    {
                        OutTrace.Log($"Ошибка при создании директории для пути {fileOutput}", ex);
                        fileOutput = null;
                    }
                }

                if (fileOutput != null)
                {
                    try
                    {
                        return TextWriter.Synchronized(new StreamWriter(fileOutput));
                    }
                    catch (Exception ex)
                    {
                        OutTrace.Log($"Ошибка при создании потока записи {fileOutput}", ex);
                    }
                }
            }
            return null;
        }, true);
        /// <summary>
        /// Объект для записи
        /// </summary>
        static TextWriter Writer {
            get
            {
                return writer.Value;
            }
        }

        /// <summary>
        /// Событие, вызываемое при очередном выводе
        /// </summary>
        public static event Action<OutputData> Traced = delegate { };

            
        static void Out(OutputData _data)
        {
            Traced(_data);

            var text = $"[{DateTime.Now.Writable()}] {_data.Text}{Environment.NewLine}";
#if DEBUG
            Debug.Write(text);
#endif
            if (Writer == null) return;
            try
            {
                Writer.Write(text);
                Writer.Flush();
            }
            catch (Exception ex)
            {
                OutTrace.Log("Ошибка при выводе", ex);
            }
        }

        /// <summary>
        /// Вывести строку
        /// </summary>
        public static void Log(string text)
        {
            var data = new OutputData(text);
            Out(data);          
        }

        /// <summary>
        /// Вывести строку с дополнительной информацией
        /// </summary>
        public static void LogAdditional(string text, string additional)
        {
            var data = new OutputData(text, additional);
            Out(data);
        }

        /// <summary>
        /// Вывести ошибку
        /// </summary>
        public static void Error(string text)
        {
            var data = new OutputData(text, OutputData.Variants.Error);
            Out(data);
        }

        /// <summary>
        /// Вывести ошибку с дополнительной информацией
        /// </summary>
        public static void ErrorAdditional(string text, string additional)
        {
            var data = new OutputData(text, OutputData.Variants.Error, additional);
            Out(data);
        }

        /// <summary>
        /// Вывести предупреждение
        /// </summary>
        public static void Warning(string text)
        {
            var data = new OutputData(text, OutputData.Variants.Warning);
            Out(data);
        }

        /// <summary>
        /// Вывести предупреждение с дополнительной информацией
        /// </summary>
        public static void WarningAdditional(string text, string additional)
        {
            var data = new OutputData(text, OutputData.Variants.Warning, additional);
            Out(data);
        }
    }
}
