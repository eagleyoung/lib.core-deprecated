﻿namespace Lib.Core
{
    /// <summary>
    /// Объект представления данных о выводе
    /// </summary>
    public struct OutputData
    {
        /// <summary>
        /// Текст для вывода
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Дополнительная информация
        /// </summary>
        public string Additional { get; }

        /// <summary>
        /// Доступные варианты вывода
        /// </summary>
        public enum Variants { Default, Warning, Error }

        /// <summary>
        /// Текущий вариант вывода
        /// </summary>
        public Variants Variant { get; }

        public OutputData(string _text, string _additional = null) : this(_text, Variants.Default, _additional) { }
        public OutputData(string _text, Variants _variant, string _additional = null)
        {
            Text = _text;
            Variant = _variant;
            Additional = _additional;
        }
    }
}
