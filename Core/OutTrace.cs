﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Lib.Core
{
    /// <summary>
    /// Вывод отладочной информации в файл. 
    /// Позволяет настроить вывод через конфигурационный файл trace.ini
    /// </summary>
    public static class OutTrace
    {
        static Lazy<TextWriter> writer = new Lazy<TextWriter>(() => {
            var assembly = Assembly.GetEntryAssembly();
            if (assembly != null)
            {
                var fileOutput = $"{Path.GetDirectoryName(assembly.Location)}\\trace\\{Environment.UserName}_{DateTime.Now.Writable()}.txt";

                var info = new FileInfo(fileOutput);
                if (!info.Directory.Exists)
                {
                    try
                    {
                        info.Directory.Create();
                    }
                    catch
                    {
                        fileOutput = null;
                        return null;
                    }
                }

                cfgFile = $"{Path.GetDirectoryName(assembly.Location)}\\settings\\trace.ini";
                var cfgInfo = new FileInfo(cfgFile);
                if (!cfgInfo.Directory.Exists)
                {
                    try
                    {
                        cfgInfo.Directory.Create();
                    }
                    catch
                    {
                        cfgFile = null;
                        return null;
                    }
                }
                if (!cfgInfo.Exists)
                {
                    try
                    {
                        using (var file = cfgInfo.CreateText()) { }
                    }
                    catch
                    {
                        cfgFile = null;
                    }
                }

                foreach (var line in File.ReadAllLines(cfgFile))
                {
                    var spl = line.Split(new char[] { '=' }, StringSplitOptions.None);
                    if (spl.Length == 2)
                    {
                        var key = spl[0].Trim();
                        bool value = false;
                        if (bool.TryParse(spl[1].Trim(), out value))
                        {
                            if (!canTrace.ContainsKey(key))
                            {
                                canTrace.Add(key, value);
                            }
                        }
                    }
                }

                if (fileOutput != null)
                {
                    try
                    {
                        return TextWriter.Synchronized(new StreamWriter(fileOutput));
                    }
                    catch { }
                }
            }
            return null;

        }, true);
        /// <summary>
        /// Объект для записи
        /// </summary>
        static TextWriter Writer {
            get
            {
                return writer.Value;
            }
        }

        static Dictionary<string, bool> canTrace = new Dictionary<string, bool>();
        
        static string cfgFile;
        
        static bool CanTrace(string key)
        {
            if (!canTrace.ContainsKey(key))
            {
                canTrace.Add(key, true);
                if(cfgFile != null)
                {
                    try
                    {
                        File.AppendAllText(cfgFile, $"{key} = True{Environment.NewLine}");
                    }
                    catch { }
                }                
            }

            return canTrace[key];
        }

        /// <summary>
        /// Событие, вызываемое при очередном выводе
        /// </summary>
        public static event Action<string> Traced = delegate { };

        /// <summary>
        /// Вывести данные в лог
        /// </summary>
        public static void Log(string text, [CallerMemberName] string name = null, [CallerFilePath] string path = null, [CallerLineNumber] int number = -1)
        {
            string key = $"{path}.{name}.{number}";

            if (!CanTrace(key)) return;
            
            Out(key, text);
        }

        /// <summary>
        /// Вывести данные об исключении в лог
        /// </summary>
        public static void Log(Exception ex, [CallerMemberName] string name = null, [CallerFilePath] string path = null, [CallerLineNumber] int number = -1)
        {
            string key = $"{path}.{name}.{number}";

            if (!CanTrace(key)) return;

            Out(key, ex.GetExceptionData());
        }

        /// <summary>
        /// Вывести данные об исключении в лог с дополнительной информацией
        /// </summary>
        public static void Log(string text, Exception ex, [CallerMemberName] string name = null, [CallerFilePath] string path = null, [CallerLineNumber] int number = -1)
        {
            string key = $"{path}.{name}.{number}";

            if (!CanTrace(key)) return;

            Out(key, $"{text}{Environment.NewLine}{ex.GetExceptionData()}");
        }

        
        static void Out(string key, string text)
        {
            var data = $"[{DateTime.Now.Writable()}] {key}{Environment.NewLine}{text}{Environment.NewLine}{Environment.StackTrace}{Environment.NewLine}";
            Traced(data);
#if DEBUG
            Debug.Write(data);
#endif
            if (Writer == null) return;

            try
            {
                Writer.Write(data);
                Writer.Flush();
            }
            catch { }
        }
    }
}
