﻿using System.Diagnostics;
using System.IO;

namespace Lib.Core
{
    /// <summary>
    /// Вспомогательные возможности
    /// </summary>
    public static class CoreHelper
    {
        /// <summary>
        /// Перевести количество байт в мегабайты
        /// </summary>
        public static double BytesToMegabytes(long bytes)
        {
            return (bytes / 1024d) / 1024d;
        }

        /// <summary>
        /// Есть доступ на запись по указанному пути?
        /// </summary>
        public static bool HasWriteAccess(string path)
        {
            try
            {
                using (FileStream fs = File.Create(Path.Combine(path, Path.GetRandomFileName()), 1, FileOptions.DeleteOnClose)) { }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Есть доступ на запись в указанную папку?
        /// </summary>
        public static bool HasWriteAccess(DirectoryInfo dir)
        {
            return HasWriteAccess(dir.FullName);
        }

        /// <summary>
        /// Запустить процесс
        /// </summary>
        /// <param name="path">Путь до файла для запуска</param>
        /// <param name="checkExistance">При наличии процесса в списке запущенных не запускает новый</param>
        public static void StartProcess(string path, bool checkExistance = true)
        {
            var info = new FileInfo(path);
            if (!info.Exists)
            {
                OutTrace.Log($"Отсутствует файл для запуска по пути {path}");
                return;
            }

            if (checkExistance)
            {
                var name = info.Name.Remove(info.Name.Length - info.Extension.Length).ToLower();
                var processes = Process.GetProcesses();
                foreach (var process in processes)
                {
                    if (process.ProcessName.ToLower() == name) return;
                }
            }
            var startInfo = new ProcessStartInfo(path);

            Process.Start(startInfo);
        }

        /// <summary>
        /// Есть процесс в списке запущенных?
        /// </summary>
        public static bool HasProcess(string name)
        {
            return Process.GetProcessesByName(name).Length > 0;
        }

        /// <summary>
        /// Закончить процесс с указанным именем
        /// </summary>
        public static void KillProcess(string name)
        {
            var processes = Process.GetProcessesByName(name);
            foreach (var process in processes)
            {
                process.Kill();
                process.WaitForExit();
                process.Dispose();
            }
        }

        /// <summary>
        /// Закончить процессы с указанными именами
        /// </summary>
        public static void KillProcesses(string[] names)
        {
            foreach (var name in names)
            {
                KillProcess(name);
            }
        }
    }
}
