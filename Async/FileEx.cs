﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Lib.Core.Async
{
    /// <summary>
    /// Класс для работы с файлами асинхронно
    /// </summary>
    public static class FileEx
    {
        const int DefaultBufferSize = 4096;
        const FileOptions DefaultOptions = FileOptions.Asynchronous | FileOptions.SequentialScan;

        /// <summary>
        /// Вывести текст файла построчно
        /// </summary>
        public static async Task<string[]> ReadAllLinesAsync(string _path, Encoding _encoding = null)
        {
            if (_encoding == null) _encoding = Encoding.Unicode;
            var lines = new List<string>();

            using(var stream = new FileStream(_path, FileMode.Open, FileAccess.Read, FileShare.Read, DefaultBufferSize, DefaultOptions))
            {
                using (var reader = new StreamReader(stream, _encoding))
                {
                    string line;
                    while((line = await reader.ReadLineAsync().ConfigureAwait(false)) != null)
                    {
                        lines.Add(line);
                    }
                }
            }

            return lines.ToArray();
        }

        /// <summary>
        /// Добавить текст в файл
        /// </summary>
        public static async Task AppendAsync(string _path, string _text)
        {
            byte[] encoded = Encoding.Unicode.GetBytes(_text);

            using(var stream = new FileStream(_path, FileMode.Append, FileAccess.Write, FileShare.None, DefaultBufferSize, DefaultOptions))
            {
                await stream.WriteAsync(encoded, 0, encoded.Length).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Копировать файл
        /// </summary>
        public static async Task CopyFileAsync(string from, string to)
        {
            using(var source = new FileStream(from, FileMode.Open, FileAccess.Read, FileShare.Read, DefaultBufferSize, DefaultOptions))
            {
                using(var destination = new FileStream(to, FileMode.CreateNew, FileAccess.Write, FileShare.None, DefaultBufferSize, DefaultOptions))
                {
                    await source.CopyToAsync(destination).ConfigureAwait(false);
                }
            }
        }
    }
}
